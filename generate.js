import fs from 'fs';
import faker from 'faker';

const data = Array.from({ length: 8232 }, () => ({
    birthday: faker.date.past(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    creditCardNumber: faker.finance.creditCardNumber(),
    cvv: faker.finance.creditCardCVV(),
}));
fs.writeFileSync('dump.json', JSON.stringify(data), 'utf8');
