FROM mongo:4.4
USER root
WORKDIR /app
EXPOSE 27017
ENV MONGO_INITDB_DATABASE=backup
COPY dump.json .
CMD mongoimport --db backup --collection users --type json --file dump.json --jsonArray & mongod --bind_ip_all

